import { TestBed } from '@angular/core/testing';

import { MercureService } from './mercure.service';

describe('MercureService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MercureService = TestBed.get(MercureService);
    expect(service).toBeTruthy();
  });
});
