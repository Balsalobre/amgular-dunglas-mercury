import { Injectable } from '@angular/core';
import { ToastService } from './toast.service';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { NbToastrService } from '@nebular/theme';

@Injectable({
  providedIn: 'root'
})
export class MercureService {

  mercureURL: string = 'http://localhost:81/hub?topic=angular';

  constructor(
    private toastService: ToastService,
    private toastrService: NbToastrService,
    private toastr: ToastrService,
    private http: HttpClient
  ) { }

  getEventSource() {
    const eventSource = new EventSource(this.mercureURL);
    
    eventSource.onmessage = e => {
      console.log(e);
      const position = 'top-end';
      this.toastrService.info(e.data, 'Mensaje desde mercure');
      // this.toastService.showToast(null, e.data);
      //this.toastr.success(null, e.data);
    }
  }
}
