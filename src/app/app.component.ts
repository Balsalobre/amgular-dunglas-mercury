import { Component } from '@angular/core';
import { ToastService } from './toast.service';
import { MercureService } from './mercure.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  
  constructor(
    private toastService: ToastService,
    private mercureService: MercureService
  ) {}

  ngOnInit() {
    this.mercureService.getEventSource();
  }

  example() {
    this.toastService.showSuccess('test', 'Hi there!');
  }
}
